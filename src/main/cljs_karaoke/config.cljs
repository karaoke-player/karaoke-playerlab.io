(ns cljs-karaoke.config)

(def ^:export config-map
  {:audio-files-prefix "https://baskeboler.github.io/cljs-karaoke"
   :app-url-prefix     "https://karaoke-player.gitlab.io"
   :http-timeout       15000})
